using Opah.TesteBackEnd.Domain.Model.Entity;
using Opah.TesteBackEnd.Domain.Model.Entity.EnumEntity;
using Opah.TesteBackEnd.Domain.Model.Request;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Opah.TesteBackEnd.Domain.Contract.Infrastructure.Repository
{
    public interface IClienteRepository : IDisposable
    {
        Task<Cliente> GetByCpfCodEmpresaAsync(string cpf, CodEmpresaEnum codEmpresa);
        Task<List<Cliente>> FilterClientesAsync(FiltroClienteRequest filtro);
        Task<Cliente> GetByIdAsync(int id);
        Task<Cliente> InsertAsync(Cliente entity);
        Task<Cliente> RemoveAsync(int id);
        Task<Cliente> UpdateAsync(Cliente entity);
    }
}
