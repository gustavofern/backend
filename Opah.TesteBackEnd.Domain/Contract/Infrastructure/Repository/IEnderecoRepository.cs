using Opah.TesteBackEnd.Domain.Model.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Opah.TesteBackEnd.Domain.Contract.Infrastructure.Repository
{
    public interface IEnderecoRepository : IDisposable
    {
        Task<List<Endereco>> GetByClienteAsync(int idCliente);
        Task<Endereco> InsertAsync(Endereco entity);
    }
}
