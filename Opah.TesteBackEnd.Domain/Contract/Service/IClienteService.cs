using Opah.TesteBackEnd.Domain.Model.Entity;
using Opah.TesteBackEnd.Domain.Model.Request;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Opah.TesteBackEnd.Domain.Contract.Service
{
    public interface IClienteService
    {
        Task<Cliente> CreateNewAsync(Cliente lead);
        Task<Cliente> UpdateAsync(Cliente cliente);
        Task<Cliente> DeleteAsync(int idLead);
        Task<Cliente> GetByIdAsync(int idLead);
        Task<List<Cliente>> FilterClientesAsync(FiltroClienteRequest filtro);
    }
}
