using Opah.TesteBackEnd.Domain.Model.Entity;
using System.Threading.Tasks;

namespace Opah.TesteBackEnd.Domain.Contract.Service
{
    public interface IEnderecoService
    {
        Task<Endereco> CreateNewAsync(Endereco endereco, int idCliente);
    }
}
