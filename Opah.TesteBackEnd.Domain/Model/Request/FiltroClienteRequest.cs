using Microsoft.AspNetCore.Mvc;
using Opah.TesteBackEnd.Domain.Model.Entity.EnumEntity;
using System.ComponentModel.DataAnnotations;

namespace Opah.TesteBackEnd.Domain.Model.Request
{
    public class FiltroClienteRequest
    {
        [FromQuery, Required]
        public CodEmpresaEnum CodEmpresa { get; set; }

        [FromQuery]
        public string Nome { get; set; }

        [FromQuery]
        public string Cpf { get; set; }

        [FromQuery]
        public string Cidade { get; set; }

        [FromQuery]
        public string Estado { get; set; }

    }
}
