using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Opah.TesteBackEnd.Domain.Model.Entity
{
    [Table("cidade")]
    public class Cidade
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(100)]
        public string Nome { get; set; } = default!;

        [MaxLength(2)]
        public string Estado { get; set; } = default!;
    }
}
