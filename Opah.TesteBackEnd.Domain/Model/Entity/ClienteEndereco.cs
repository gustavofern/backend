using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Opah.TesteBackEnd.Domain.Model.Entity
{
    [Table("cliente_endereco")]
    public class ClienteEndereco
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey(nameof(Cliente))]
        public int IdCliente { get; set; }

        [ForeignKey(nameof(Endereco))]
        public int IdEndereco { get; set; }

        public Cliente Cliente { get; set; }
        public Endereco Endereco { get; set; }
    }
}
