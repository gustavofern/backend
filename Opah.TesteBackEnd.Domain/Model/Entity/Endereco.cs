using Opah.TesteBackEnd.Domain.Model.Entity.EnumEntity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Opah.TesteBackEnd.Domain.Model.Entity
{
    [Table("endereco")]
    public class Endereco
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey(nameof(Cidade))]
        public int IdCidade { get; set; }

        [MaxLength(255)]
        public string Rua { get; set; } = default!;

        [MaxLength(50)]
        public string Bairro { get; set; } = default!;

        [MaxLength(50)]
        public string Numero { get; set; } = default!;

        [MaxLength(100)]
        public string Complemento { get; set; }

        [MaxLength(10)]
        public string Cep { get; set; } = default!;
        public TipoEnderecoEnum TipoEndereco { get; set; } = default!;

        public Cidade Cidade { get; set; }

        public List<ClienteEndereco> ClienteEndereco { get; } = new List<ClienteEndereco>();
    }
}
