using Opah.TesteBackEnd.Domain.Model.Entity.EnumEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Opah.TesteBackEnd.Domain.Model.Entity
{
    [Table("cliente")]
    public class Cliente
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(150)]
        public string Nome { get; set; } = default!;

        [MaxLength(150)]
        public string Email { get; set; } = default!;

        [MaxLength(20)]
        public string RG { get; set; } = default!;

        [MaxLength(20)]
        public string CPF { get; set; } = default!;

        [MaxLength(20)]
        public string Telefone { get; set; } = default!;

        public DateTime DataNascimento { get; set; } = default!;

        public CodEmpresaEnum CodEmpresa { get; set; }

        public List<ClienteEndereco> ClienteEndereco { get; } = new List<ClienteEndereco>(); 
    }
}
