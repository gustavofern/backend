namespace Opah.TesteBackEnd.Domain.Model.Entity.EnumEntity
{
    public enum TipoEnderecoEnum
    {
        EnderecoResidencial = 1,
        EnderecoComercial = 2,
        Outros = 3
    }
}
