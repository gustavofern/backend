using Opah.TesteBackEnd.Domain.Contract.Infrastructure.Repository;
using Opah.TesteBackEnd.Domain.Model.Entity;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System;
using Opah.TesteBackEnd.Domain.Model.Entity.EnumEntity;
using Opah.TesteBackEnd.Domain.Model.Request;
using System.Collections.Generic;
using System.Linq;

namespace Opah.TesteBackEnd.Infra.Entity.Repository
{
    public class ClienteRepository : IClienteRepository
    {
        private readonly EFDbContext context;
        private readonly DapperDbContext dapperDbContext;
        protected readonly DbSet<Cliente> dbSet;
        private readonly ILogger<ClienteRepository> logger;

        private bool disposed;
        public ClienteRepository(EFDbContext context,
                                 DapperDbContext dapperDbContext,
                                 ILogger<ClienteRepository> logger)
        {
            this.context = context;
            this.dapperDbContext = dapperDbContext;
            dbSet = this.context.Set<Cliente>();
            this.logger = logger;
        }

        public virtual async Task<Cliente> GetByIdAsync(int id)
        {
            return await dbSet.Include(c => c.ClienteEndereco).ThenInclude(ce => ce.Endereco).FirstOrDefaultAsync(c => c.Id == id);
        }

        public virtual async Task<List<Cliente>> FilterClientesAsync(FiltroClienteRequest filtro)
        {
            var clientes = new List<Cliente>();

            using (var command = dapperDbContext.CreateCommand())
            {
                var query = @"select c.* from cliente c
                                                        left join clienteendereco ce on ce.idCliente = c.id
                                                        left join endereco e on e.id = ce.idEndereco
                                                        left join cidade ci on ci.id = e.idCidade  ";

                query += $"where c.codEmpresa = @codEmpresa";
                command.Parameters.AddWithValue("codEmpresa", filtro.CodEmpresa);

                if (!string.IsNullOrEmpty(filtro.Cidade))
                {
                    query += $" and ci.nome = @cidade";
                    command.Parameters.AddWithValue("cidade", filtro.Cidade);
                }
                if (!string.IsNullOrEmpty(filtro.Cidade))
                {
                    query += $" and ci.nome = @cidade";
                    command.Parameters.AddWithValue("cidade", filtro.Cidade);
                }
                if (!string.IsNullOrEmpty(filtro.Cidade))
                {
                    query += $" and ci.nome = @cidade";
                    command.Parameters.AddWithValue("cidade", filtro.Cidade);
                }
                if (!string.IsNullOrEmpty(filtro.Cidade))
                {
                    query += $" and ci.nome = @cidade";
                    command.Parameters.AddWithValue("cidade", filtro.Cidade);
                }

                query += " group by c.id order by c.nome";

                command.CommandText = query;

                using (var reader = await command.ExecuteReaderAsync())
                {
                    while (reader.Read())
                    {
                        var cliente = new Cliente
                        {
                            Id = Convert.ToInt32(reader["id"]),
                            CodEmpresa = (CodEmpresaEnum)Convert.ToInt32(reader["codEmpresa"]),
                            CPF = reader["cpf"].ToString(),
                            DataNascimento = Convert.ToDateTime(reader["dataNascimento"]),
                            Email = reader["email"].ToString(),
                            Nome = reader["nome"].ToString(),
                            RG = reader["rg"].ToString(),
                            Telefone = reader["telefone"].ToString()
                        };
                        clientes.Add(cliente);
                    };

                }
            }

            return clientes;
        }

        public virtual async Task<Cliente> GetByCpfCodEmpresaAsync(string cpf, CodEmpresaEnum codEmpresa)
        {
            return await dbSet.FirstOrDefaultAsync(c => c.CPF.Equals(cpf, StringComparison.InvariantCultureIgnoreCase) && c.CodEmpresa.Equals(codEmpresa));
        }

        public virtual async Task<Cliente> InsertAsync(Cliente entity)
        {
            logger.LogInformation($"Inserindo entidade do repositório Cliente");
            dbSet.Add(entity);
            return await CommitAsync(entity);
        }

        public virtual async Task<Cliente> RemoveAsync(int id)
        {
            logger.LogInformation($"Removendo entidade do repositório Cliente");
            var entity = await dbSet.FindAsync(id);
            dbSet.Remove(entity);
            return await CommitAsync(entity);
        }

        public virtual async Task<Cliente> UpdateAsync(Cliente entity)
        {
            logger.LogInformation($"Atualizando entidade no repositório Cliente");
            dbSet.Update(entity);
            return await CommitAsync(entity);
        }

        private async Task<Cliente> CommitAsync(Cliente entity)
        {
            logger.LogInformation("Realizando commit.");
            var result = await context.SaveChangesAsync();
            if (result > 0)
            {
                return entity;
            }
            return null;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
