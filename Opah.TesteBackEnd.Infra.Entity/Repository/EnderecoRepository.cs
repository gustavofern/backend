using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Opah.TesteBackEnd.Domain.Contract.Infrastructure.Repository;
using Opah.TesteBackEnd.Domain.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opah.TesteBackEnd.Infra.Entity.Repository
{
    public class EnderecoRepository : IEnderecoRepository
    {
        private readonly EFDbContext context;
        protected readonly DbSet<Endereco> dbSet;
        private readonly ILogger<EnderecoRepository> logger;

        private bool disposed;
        public EnderecoRepository(EFDbContext context,
                                  ILogger<EnderecoRepository> logger)
        {
            this.context = context;
            dbSet = this.context.Set<Endereco>();
            this.logger = logger;
        }

        public virtual async Task<List<Endereco>> GetByClienteAsync(int idCliente)
        {
            return await dbSet.Where(e => e.ClienteEndereco.Any(ce => ce.IdCliente == idCliente)).ToListAsync();
        }

        public virtual async Task<Endereco> InsertAsync(Endereco entity)
        {
            logger.LogInformation($"Inserindo entidade do repositório Endereco");
            dbSet.Add(entity);
            return await CommitAsync(entity);
        }

        private async Task<Endereco> CommitAsync(Endereco entity)
        {
            logger.LogInformation("Realizando commit.");
            var result = await context.SaveChangesAsync();
            if (result > 0)
            {
                return entity;
            }
            return null;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
