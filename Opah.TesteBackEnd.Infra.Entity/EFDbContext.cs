using Opah.TesteBackEnd.Domain.Model.Entity;
using Microsoft.EntityFrameworkCore;

namespace Opah.TesteBackEnd.Infra.Entity
{
    public class EFDbContext : DbContext
    {
        public EFDbContext(DbContextOptions<EFDbContext> options) : base(options) { }
        public virtual DbSet<Cliente> Cliente { get; set; } = default!;
        public virtual DbSet<ClienteEndereco> ClienteEndereco { get; set; } = default!;
        public virtual DbSet<Endereco> Endereco { get; set; } = default!;
        public virtual DbSet<Cidade> Cidade { get; set; } = default!;
    }
}
