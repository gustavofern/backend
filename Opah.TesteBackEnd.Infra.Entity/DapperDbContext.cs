using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;

namespace Opah.TesteBackEnd.Infra.Entity
{
    public class DapperDbContext : IDisposable
    {
        private readonly MySqlConnection _connection;
        private MySqlTransaction _sqlTransaction;
        private bool disposed;
        private readonly IConfiguration _configuration;
        public DapperDbContext(IConfiguration configuration)
        {
            _configuration = configuration;
            _connection = Create();
        }

        private MySqlConnection Create()
        {

            var conStr = _configuration.GetConnectionString("DefaultConnection");
            MySqlConnection connection = new MySqlConnection(conStr);
            if (connection == null)
                throw new Exception(string.Format((string)"Failed to create a connection using the connection string named '{0}' in appSettings.json.", (object)conStr));

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

            }
            catch (Exception)
            {
                throw;
            }

            return connection;
        }

        public void BeginTransaction()
        {
            _sqlTransaction = _connection.BeginTransaction();
        }

        public MySqlConnection GetConnection()
        {
            return _connection;
        }

        public MySqlCommand CreateCommand()
        {
            var cmd = _connection.CreateCommand();
            cmd.Transaction = _sqlTransaction;

            return cmd;
        }

        public void Commit()
        {
            if (_sqlTransaction == null)
                return;

            _sqlTransaction.Commit();
            _sqlTransaction = null;
        }
        public void LimpaPool()
        {
            _connection.ClearPoolAsync(_connection);
        }

        public bool IsBeginTrasactionOpen()
        {
            return _sqlTransaction != null;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (_sqlTransaction != null)
                    {
                        _sqlTransaction.Rollback();
                        _sqlTransaction.Dispose();
                        _sqlTransaction = null;
                    }
                    if (_connection.State == System.Data.ConnectionState.Open)
                    {
                        _connection.ClearPoolAsync(_connection);
                        _connection.Close();
                        _connection.Dispose();
                    }
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
