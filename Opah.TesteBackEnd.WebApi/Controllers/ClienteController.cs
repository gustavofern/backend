using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Opah.TesteBackEnd.Domain.Contract.Service;
using Opah.TesteBackEnd.Domain.Model.Entity;
using Opah.TesteBackEnd.Domain.Model.Request;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Opah.TesteBackEnd.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        private readonly IClienteService clienteService;
        private readonly IEnderecoService enderecoService;
        private readonly ILogger<ClienteController> logger;

        public ClienteController(IClienteService clienteService, IEnderecoService enderecoService, ILogger<ClienteController> logger)
        {
            this.clienteService = clienteService;
            this.enderecoService = enderecoService;
            this.logger = logger;
        }

        [HttpGet("lista")]

        public async Task<IActionResult> ListAsync([FromQuery] FiltroClienteRequest filtro)
        {
            logger.LogInformation($"Obtendo clientes");
            return Ok(await clienteService.FilterClientesAsync(filtro));
        }

        [HttpGet("{idCliente}")]
        public async Task<IActionResult> GetByIdAsync([Required] int idCliente)
        {
            logger.LogInformation($"Obtendo cliente com o ID {idCliente}");
            return Ok(await clienteService.GetByIdAsync(idCliente));
        }

        [HttpPost]
        public async Task<IActionResult> CreateNewAsync([Required, FromBody] Cliente cliente)
        {
            logger.LogInformation($"Criando o cliente {cliente.Nome}");
            return Ok(await clienteService.CreateNewAsync(cliente));
        }

        [HttpPut]
        public async Task<IActionResult> UpdateNameAsync([Required, FromBody] Cliente cliente)
        {
            logger.LogInformation($"Atualizando o cliente {cliente.Nome}");
            return Ok(await clienteService.UpdateAsync(cliente));
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteAsync([Required] int idCliente)
        {
            logger.LogInformation($"Removendo o cliente {idCliente}");
            return Ok(await clienteService.DeleteAsync(idCliente));
        }

        [HttpPut("Endereco/{idCliente}")]
        public async Task<IActionResult> AddAddressAsync([Required, FromBody] Endereco endereco, int idCliente)
        {
            return Ok(await enderecoService.CreateNewAsync(endereco, idCliente));
        }
    }
}
