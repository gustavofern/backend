using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using Thinktecture;
using Thinktecture.Extensions.Configuration;

namespace Opah.TesteBackEnd.WebApi
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = "Opah.TesteBackEnd";
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    var loggingConfig = new LoggingConfiguration();
                    webBuilder.ConfigureServices(config => config.AddSingleton<ILoggingConfiguration>(loggingConfig));
                    webBuilder.ConfigureAppConfiguration(config =>
                    {
                        config.AddLoggingConfiguration(loggingConfig, "Logging");
                    });
                });
    }
}
