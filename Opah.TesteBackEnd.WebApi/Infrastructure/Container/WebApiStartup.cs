using Opah.TesteBackEnd.Domain.Contract.Infrastructure.Repository;
using Opah.TesteBackEnd.Domain.Contract.Service;
using Opah.TesteBackEnd.Infra.Entity;
using Opah.TesteBackEnd.Infra.Entity.Repository;
using Opah.TesteBackEnd.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Globalization;

namespace Opah.TesteBackEnd.WebApi.Infrastructure.Container
{
    public static class WebApiStartup
    {
        private const string apiCulture = "WebapiCulture";

        public static RequestLocalizationOptions Location(IConfiguration configuration) => new RequestLocalizationOptions
        {
            SupportedCultures = new List<CultureInfo> { new CultureInfo(configuration[apiCulture]) },
            SupportedUICultures = new List<CultureInfo> { new CultureInfo(configuration[apiCulture]) },
            DefaultRequestCulture = new RequestCulture(configuration[apiCulture])
        };

        public static void Register(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<EFDbContext>(options => options.UseMySQL(configuration.GetConnectionString("DefaultConnection")),
                                                 ServiceLifetime.Transient);
            services.AddTransient<DapperDbContext>();
            RegisterServices(services);
            RegisterRepositories(services);
        }

        private static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IClienteService, ClienteService>();
            services.AddScoped<IEnderecoService, EnderecoService>();
        }

        private static void RegisterRepositories(IServiceCollection services)
        {
            services.AddScoped<IClienteRepository, ClienteRepository>();
            services.AddScoped<IEnderecoRepository, EnderecoRepository>();
        }
    }
}
