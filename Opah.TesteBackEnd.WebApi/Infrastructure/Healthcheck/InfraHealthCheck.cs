using Opah.TesteBackEnd.Infra.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Opah.TesteBackEnd.WebApi.Infrastructure.Healthcheck
{
    public class InfraHealthCheck : IHealthCheck
    {
        private readonly EFDbContext kulaDbContext;
        public InfraHealthCheck(EFDbContext kulaDbContext) => this.kulaDbContext = kulaDbContext;

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            try
            {
                await kulaDbContext.Database.OpenConnectionAsync(default);
                await kulaDbContext.Database.CloseConnectionAsync();
            }
            catch (Exception ex)
            {
                return new HealthCheckResult(status: context.Registration.FailureStatus, exception: ex);
            }
            return HealthCheckResult.Healthy();
        }
    }
}
