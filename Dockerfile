#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["Opah.TesteBackEnd.WebApi/Opah.TesteBackEnd.WebApi.csproj", "Opah.TesteBackEnd.WebApi/"]
COPY ["Opah.TesteBackEnd.Infra.Entity/Opah.TesteBackEnd.Infra.Entity.csproj", "Opah.TesteBackEnd.Infra.Entity/"]
COPY ["Opah.TesteBackEnd.Domain/Opah.TesteBackEnd.Domain.csproj", "Opah.TesteBackEnd.Domain/"]
COPY ["Opah.TesteBackEnd.Service/Opah.TesteBackEnd.Service.csproj", "Opah.TesteBackEnd.Service/"]
RUN dotnet restore "Opah.TesteBackEnd.WebApi/Opah.TesteBackEnd.WebApi.csproj"
COPY . .
WORKDIR "/src/Opah.TesteBackEnd.WebApi"
RUN dotnet build "Opah.TesteBackEnd.WebApi.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Opah.TesteBackEnd.WebApi.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Opah.TesteBackEnd.WebApi.dll"]