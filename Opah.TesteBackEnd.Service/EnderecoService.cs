using Microsoft.Extensions.Logging;
using Opah.TesteBackEnd.Domain.Contract.Infrastructure.Repository;
using Opah.TesteBackEnd.Domain.Contract.Service;
using Opah.TesteBackEnd.Domain.Model.Entity;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Opah.TesteBackEnd.Service
{
    public class EnderecoService : IEnderecoService
    {
        private readonly IEnderecoRepository enderecoRepository;
        private readonly ILogger<EnderecoService> logger;

        public EnderecoService(IEnderecoRepository enderecoRepository,
                           ILogger<EnderecoService> logger)
        {
            this.logger = logger;
            this.enderecoRepository = enderecoRepository;
        }

        public async Task<Endereco> CreateNewAsync(Endereco endereco, int idCliente)
        {
            logger.LogInformation($"Criando um novo endereco...");
            var enderecoSalvo = await enderecoRepository.GetByClienteAsync(idCliente);

            if (enderecoSalvo != null && enderecoSalvo.Any(e => e.TipoEndereco.Equals(endereco.TipoEndereco)))
            {
                throw new Exception("Esse cliente já tem um endereço deste tipo.");
            }

            endereco.ClienteEndereco.Add(new ClienteEndereco { IdCliente = idCliente });

            return await enderecoRepository.InsertAsync(endereco);
        }

    }
}
