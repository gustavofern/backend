using Opah.TesteBackEnd.Domain.Contract.Infrastructure.Repository;
using Opah.TesteBackEnd.Domain.Contract.Service;
using Opah.TesteBackEnd.Domain.Model.Entity;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Opah.TesteBackEnd.Domain.Model.Request;

namespace Opah.TesteBackEnd.Service
{
    public class ClienteService : IClienteService
    {
        private readonly IClienteRepository clienteRepository;
        private readonly ILogger<ClienteService> logger;

        public ClienteService(IClienteRepository clienteRepository,
                           ILogger<ClienteService> logger)
        {
            this.logger = logger;
            this.clienteRepository = clienteRepository;
        }

        public async Task<Cliente> CreateNewAsync(Cliente cliente)
        {
            logger.LogInformation($"Criando um novo cliente...");
            var clienteSalvo = await clienteRepository.GetByCpfCodEmpresaAsync(cliente.CPF, cliente.CodEmpresa);

            if (clienteSalvo != null)
            {
                throw new Exception("Já existe um cliente com este CPF nessa empresa.");
            }

            return await clienteRepository.InsertAsync(cliente);
        }

        public async Task<Cliente> UpdateAsync(Cliente cliente)
        {
            logger.LogInformation($"Criando um novo cliente...");
            var clienteSalvo = await clienteRepository.GetByIdAsync(cliente.Id);

            if (clienteSalvo == null)
            {
                throw new Exception("Cliente não encontrado.");
            }

            return await clienteRepository.UpdateAsync(cliente);
        }

        public async Task<Cliente> DeleteAsync(int idLead)
        {
            logger.LogInformation($"Removendo a cliente {idLead}...");
            return await clienteRepository.RemoveAsync(idLead);
        }

        public async Task<Cliente> GetByIdAsync(int idLead)
        {
            logger.LogInformation($"Obtendo cliente de id: {idLead}");
            return await clienteRepository.GetByIdAsync(idLead);
        }

        public async Task<List<Cliente>> FilterClientesAsync(FiltroClienteRequest filtro)
        {
            logger.LogInformation($"Obtendo clientes");
            return await clienteRepository.FilterClientesAsync(filtro);
        }
    }
}
