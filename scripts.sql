CREATE SCHEMA `testeopah`;

CREATE TABLE `testeopah`.`cliente` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(150) NOT NULL,
  `email` VARCHAR(150) NOT NULL,
  `rg` VARCHAR(20) NOT NULL,
  `cpf` VARCHAR(20) NOT NULL,
  `telefone` VARCHAR(20) NOT NULL,
  `dataNascimento` DATE NOT NULL,
  `codEmpresa` INT NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `testeopah`.`cidade` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `estado` VARCHAR(2) NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `testeopah`.`endereco` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `idCidade` INT NOT NULL,
  `rua` VARCHAR(255) NOT NULL,
  `bairro` VARCHAR(50) NOT NULL,
  `numero` VARCHAR(50) NOT NULL,
  `complemento` VARCHAR(100) NULL,
  `cep` VARCHAR(10) NOT NULL,
  `tipoEndereco` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_endereco_cidade`
    FOREIGN KEY (`idCidade`)
    REFERENCES `testeopah`.`cidade` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `testeopah`.`clienteendereco` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `idCliente` INT NOT NULL,
  `idEndereco` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_clienteendereco_cliente`
    FOREIGN KEY (`idCliente`)
    REFERENCES `testeopah`.`cliente` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_clienteendereco_endereco`
    FOREIGN KEY (`idEndereco`)
    REFERENCES `testeopah`.`endereco` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
